import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Modal, TouchableOpacity, ScrollView } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class List extends React.Component {

    state = {
        isShowModal: false,
        username: '',
        message: ''
    };

    goToScreen1 = () => {
        this.props.history.push('/login', {
        })
    }

    goToAddProduct = () => {
        this.props.history.push('/addproduct', {
        })
    }

    goToProfile = () => {
        this.props.history.push('/profile', {
           myusername: this.props.location.state.myusername,
           mypassword: this.props.location.state.mypassword,
        })
    }

    onShowModal() {
        this.setState({ isShowModal: true });
    }

    onHideModal() {
        this.setState({ isShowModal: false });
    }

    render() {
        return (

            <ScrollView contentContainerStyle={styles.contentContainer}>
                <Modal
                    visible={this.state.isShowModal}
                    transparent={true}
                >
                    <TouchableOpacity
                        onPress={() => { this.onHideModal() }}
                        style={[styles.modalLayout, styles.center]}
                    >
                        <Text style={styles.textStyle}>FOODS</Text>

                    </TouchableOpacity>
                </Modal>

                <View style={styles.header}>
                    <Text style={styles.headerText}>Foods</Text>
                </View>

                <View style={styles.menuBar}>
                    <View style={styles.menuBack}>
                        <TouchableOpacity onPress={this.goToScreen1}>
                            <Text style={styles.text}>Back</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.menuAdd}>
                        <TouchableOpacity onPress={this.goToAddProduct}>
                            <Text style={styles.text}>Add</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.menuProfile}>
                        <TouchableOpacity onPress={this.goToProfile}>
                            <Text style={styles.text}>Profile</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.content}>


                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img1.gif')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img2.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img3.png')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img4.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img5.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img6.png')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img7.png')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img8.png')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img9.png')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img10.png')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img11.gif')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>

                        <View style={styles.box2}>
                            <TouchableOpacity
                                onPress={() => { this.onShowModal() }}
                            >

                                <Image source={require('./img12.jpg')} style={styles.logo} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.footer}>
                        <View style={styles.menuBar}>
                            <View style={styles.menuBack}>
                                <TouchableOpacity onPress={this.goToScreen1}>
                                    <Text style={styles.text}>Back</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.menuAdd}>
                                <TouchableOpacity onPress={this.goToAddProduct}>
                                    <Text style={styles.text}>Add</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={styles.menuProfile}>
                                <TouchableOpacity onPress={this.goToProfile}>
                                    <Text style={styles.text}>Profile</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>

        );
    }
}


const styles = StyleSheet.create({

    contentContainer: {
        paddingVertical: 20
    },

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#82E0AA',
        alignItems: 'center'
    },

    menuBar: {
        backgroundColor: '#FCF3CF',
        flexDirection: 'row'
    },

    menuBack: {
        backgroundColor: '#82E0AA',
        flex: 0.5,
        margin: 4,
        padding: 1,
    },

    menuAdd: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 4,
        padding: 1
    },

    menuProfile: {
        backgroundColor: '#82E0AA',
        flex: 0.5,
        margin: 4,
        padding: 1
    },

    headerText: {
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold',
        padding: 15
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'column'
    },

    box1: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    row: {
        backgroundColor: '#85C1E9',
        flex: 1,
        margin: 14,
        flexDirection: 'row'
    },

    footer: {
        backgroundColor: '#CD6155',
        alignItems: 'center',
        flexDirection: 'row'
    },

    logo: {
        borderRadius: 30,
        width: 150,
        height: 150
    },
    modalLayout: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    },

    textStyle: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center'
    },

    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
        textAlign: 'center'
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    }


})
export default List