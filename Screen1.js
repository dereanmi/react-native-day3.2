import React, { Component } from 'react'
import { View, Text, Alert } from 'react-native'
import { Link } from 'react-router-native'

class Screen1 extends Component {

    UNSAFE_componentWillMount() { //เป็นcomponentที่จะถูกรันเป็นอย่างแรก เมื่อเข้าscreen1 ในconsoleจะแสดง Easy easy/this props
        //console.log('Easy easy') 
        //หรือ
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.mymessage) {
            Alert.alert ('Notification',this.props.location.state.mymessage + '')
        }
    }
 
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#F1948A' }}>
             <Text style={{ color: 'white', textAlign: 'center', fontSize: 30 }}>Screen 1</Text>
                <Link to="/screen2">
                    <Text style={{ color: 'white', textAlign: 'center', fontSize: 20 }}>Go to Screen 2</Text>
                </Link>
            </View>
        )
    }
}
export default Screen1