import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, Modal, TouchableOpacity } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Edit extends React.Component {

    state = {
       
        username: '',
        inputUsername: '',
        inputPassword: '',
        inputFirstname: '',
        inputLastname: '',
        message: ''
    };

    goToProfile = () => {
        this.props.history.push('/profile', {
            myusername: this.props.location.state.myusername,
            mypassword: this.props.location.state.mypassword,
            myfirstname: this.props.location.state.myfirstname,
            mylastname: this.props.location.state.mylastname
        })
    }

    onValueChange = (field, value) => {
        this.setState({ [field]: value });
    }

    onEdit = () => {
        this.props.history.push('/profile', {
            myusername: this.state.inputUsername,
            mypassword: this.state.inputPassword,
            myfirstname: this.state.inputFirstname,
            mylastname: this.state.inputLastname,
        })
    }

    render() {
        return (

            <View style={styles.container} >
                <View style={styles.header}>
                    <View style={styles.back}>
                        <TouchableOpacity onPress={this.goToProfile}>
                            <Text style={[styles.headerText, styles.center]}>Back</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.profileBar}>
                        <Text style={[styles.headerText, styles.center]}>Edit Profile</Text>
                    </View>
                </View>

                <View style={styles.content}>
                    <View style={styles.row}>

                        <View style={styles.box1}>
                            <Text style={[styles.headerText, styles.center]}>Username: {this.props.location.state.myusername}</Text>
                            <Text style={[styles.headerText, styles.center]}>Password: {this.props.location.state.mypassword}</Text>

                            <Text style={[styles.headerText, styles.center]}>Change username:</Text>
                            <View style={[styles.textInput, styles.center, styles.radius]}>
                                <TextInput
                                    placeholder='Username'
                                    onChangeText={value => { this.onValueChange("inputUsername", value) }} >
                                </TextInput>
                            </View>

                            <Text style={[styles.headerText, styles.center]}>Change password:</Text>
                            <View style={[styles.textInput, styles.center, styles.radius]}>
                                <TextInput
                                    placeholder='Password'
                                    onChangeText={value => { this.onValueChange("inputPassword", value) }} >
                                </TextInput>
                            </View>

                            <Text style={[styles.headerText, styles.center]}>Input Firstname:</Text>
                            <View style={[styles.textInput, styles.center, styles.radius]}>
                                <TextInput
                                    placeholder='Firstname'
                                    onChangeText={(inputFirstname) => this.setState({ inputFirstname })}>
                                </TextInput>
                            </View>
                            <Text style={[styles.headerText, styles.center]}>Input Lastname:</Text>
                            <View style={[styles.textInput, styles.center, styles.radius]}>
                                <TextInput
                                    placeholder='Lastname'
                                    onChangeText={(inputLastname) => this.setState({ inputLastname })}>
                                </TextInput>
                            </View>
                        </View>
                    </View>



                </View>
                <View style={styles.footer}>
                    <View style={styles.save}>
                        <TouchableOpacity onPress={this.onEdit}>
                            <Text style={styles.headerText}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({

    container: {
        backgroundColor: '#5DADE2',
        flex: 1
    },

    header: {
        backgroundColor: '#FCF3CF',
        alignItems: 'center',
        flexDirection: 'row'
    },

    profileBar: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 4,
        padding: 20
    },

    back: {
        backgroundColor: '#82E0AA',
        flex: 0.2,
        margin: 4,
        padding: 20

    },

    save: {
        backgroundColor: '#82E0AA',
        flex: 1,
        margin: 10,
        padding: 20

    },

    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },

    content: {
        backgroundColor: '#F8C471',
        flex: 1,
        flexDirection: 'row'
    },

    box1: {
        flex: 1,
        margin: 14,
        // alignItems: 'center',
        // justifyContent: 'center'
    },

    textInput: {
        backgroundColor: '#FCF3CF',
        // //flex: 1,
        // padding: 10,
        // // margin: 50,

    },

    footer: {
        backgroundColor: '#FCF3CF',
        alignItems: 'center',
        flexDirection: 'row'
    },

    row: {
        backgroundColor: '#85C1E9',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
    },

    logo: {
        borderRadius: 30,
        width: 150,
        height: 150
    },
    modalLayout: {
        position: 'absolute',
        top: 20,
        right: 20,
        left: 20,
        bottom: 20,
        backgroundColor: 'rgba(0,0,0,0.6)',
        flex: 1
    },
    textStyle: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    radius: {
        borderRadius: 50,
        width: 350,
        height: 70
    }


})
export default Edit
